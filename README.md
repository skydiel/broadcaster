# BROADCASTER

A simple pub/sub implementation using PostgreSQL as a message broker.

## Contribute

This library works both with Python3.

Download this repository, enter the project's root folder and follow the steps below:

* create a virtual environment with your favorite tool and make it active. For example: 
```
python3 -m venv .venv
source .venv/bin/activate
```
* install the project with 
```
make install
```

### Testing

In the project's root folder, type: 
```
make tests
```

## Distribute

In the project's root folder, type: 
```
make build
```

This will create two archives in the sub-folder `build`:
* a source distribution: broadcaster.\<version\>.tar.gz
* a built distribution: broadcaster.\<version\>-py3.none-any.whl

Both archives can be installed using: 
```
pip install <archive>
```

## Usage

After having the library installed in you project, you can:
```
import broadcaster

```
