def get_postgres_dsn(config):
    host = config['BROADCASTER_PUBSUB_PSQL_HOST']
    user = config['BROADCASTER_PUBSUB_PSQL_USERNAME']
    _pass = config['BROADCASTER_PUBSUB_PSQL_PASSWORD']
    db = config['BROADCASTER_PUBSUB_PSQL_DB']
    return f'postgres://{user}:{_pass}@{host}/{db}'
