import psycopg2 as pg
import selectors
from . import config
from ._utils import get_postgres_dsn


def publish(channel, message):
    dsn = get_postgres_dsn(config)
    with pg.connect(dsn) as conn, conn.cursor() as cursor:
        cursor.execute(f"""
            INSERT INTO broadcaster.{channel} (body) VALUES (%s)
        """, (message,))
        conn.commit()


def subscribe(channel, action, subscriber_reference):
    dsn = get_postgres_dsn(config)
    print(f"subscribing channel {channel} on host {config['BROADCASTER_PUBSUB_PSQL_HOST']}")

    with pg.connect(dsn) as conn, conn.cursor() as cursor:
        conn.set_isolation_level(pg.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

        cursor.execute(f"LISTEN {channel};")

        sel = selectors.DefaultSelector()
        sel.register(conn, selectors.EVENT_READ | selectors.EVENT_WRITE)

        while True:
            sel.select()
            conn.poll()
            while conn.notifies:
                notify = conn.notifies.pop(0)
                message_id = notify.payload

                with conn.cursor() as job_cur:
                    job_cur.execute(f'''
                        INSERT INTO broadcaster.{channel}_subscribers
                          (channel_id, reference) VALUES (%s, %s)
                    ''', (message_id, subscriber_reference))

                    job_cur.execute(f'''
                          SELECT body
                          FROM broadcaster.{channel}
                          WHERE id=%s
                    ''', (message_id,))

                    conn.commit()

                    action(job_cur.fetchone()[0])
