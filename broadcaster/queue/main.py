# import multiprocessing as mp
import psycopg2 as pg
import selectors
from . import config
from ._utils import get_postgres_dsn


def put(queue, message):
    dsn = get_postgres_dsn(config)
    with pg.connect(dsn) as conn, conn.cursor() as cursor:
        cursor.execute(f"""
            INSERT INTO broadcaster.{queue} (body) VALUES (%s)
        """, (message,))
        conn.commit()

    print(f"put {message} on queue {queue} on host {config['BROADCASTER_QUEUE_PSQL_HOST']}")


def get(queue, action, worker_ref=''):
    def get_job(job_id=None):
        with conn.cursor() as job_cur:
            sql = f'''
                UPDATE broadcaster.{queue} SET status='delivered'
                WHERE id in (
                  SELECT id
                  FROM broadcaster.{queue}
                  WHERE status='new' __ID_FILTER__
                  ORDER BY id
                  FOR UPDATE SKIP LOCKED
                )
                RETURNING id, body;
            '''
            id_filter = f'and id={job_id}' if job_id else ''
            sql = sql.replace('__ID_FILTER__', id_filter)

            job_cur.execute(sql)
            conn.commit()

            if job_cur.rowcount:
                job_id, message = job_cur.fetchone()

                action(message)

                job_cur.execute(f'''
                    UPDATE broadcaster.{queue} SET status='success'
                     WHERE id=%s;
                ''', (job_id,))

            conn.commit()

    dsn = get_postgres_dsn(config)
    print(f"subscribing queue {queue} on host {config['BROADCASTER_QUEUE_PSQL_HOST']}")

    with pg.connect(dsn) as conn, conn.cursor() as cursor:
        conn.set_isolation_level(pg.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

        cursor.execute(f"LISTEN {queue};")

        sel = selectors.DefaultSelector()
        sel.register(conn, selectors.EVENT_READ | selectors.EVENT_WRITE)

        # Try to get any pending job
        get_job()

        print(f"Waiting for notifications on queue '{queue}'")
        while True:
            sel.select()
            conn.poll()
            while conn.notifies:
                notify = conn.notifies.pop(0)
                job_id = notify.payload
                print(f"Will try to get message {job_id}")

                get_job(job_id)
