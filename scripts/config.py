import click
import psycopg2 as pg


@click.group()
def config():
    pass


@config.command()
@click.option('--host', prompt='Hostname of the PostgreSQL', help='Hostname of the PostgreSQL instance with optional port')
@click.option('--username', prompt='Instance username', help='Username to login at the PostgreSQL instance')
@click.option('--password', prompt='password', help='Password')
@click.option('--db', prompt='Database', help='Database where broadcaster will keep it information.')
@click.option('--channels', prompt='Channels', help='A comma-separated list of possible channels or topics.')
def pubsub(host, username, password, db, channels):
    try:
        conn = pg.connect(f'postgres://{username}:{password}@{host}/{db}')
    except Exception as ex:
        print(f'{ex.__class__.__name__}: {str(ex)}')
        exit(1)

    channel_list = map(lambda c: c.strip(), channels.split(','))

    with conn.cursor() as cursor:
        sql = "CREATE SCHEMA IF NOT EXISTS broadcaster;"

        for channel in channel_list:
            sql += f"""
                CREATE TABLE broadcaster.{channel} (
                    id SERIAL PRIMARY KEY,
                    body TEXT NOT NULL
                );

                CREATE TABLE broadcaster.{channel}_subscribers (
                    id SERIAL PRIMARY KEY,
                    channel_id INT NOT NULL REFERENCES broadcaster.{channel} (id),
                    reference TEXT
                );

                CREATE OR REPLACE FUNCTION {channel}_message_arrival_notify()
                    RETURNS trigger AS
                $$
                BEGIN
                    PERFORM pg_notify('{channel}', NEW.id::text);
                    RETURN NEW;
                END;
                $$ LANGUAGE plpgsql;

                CREATE TRIGGER {channel}_message_received
                    AFTER INSERT ON broadcaster.{channel}
                    FOR EACH ROW
                    EXECUTE FUNCTION {channel}_message_arrival_notify();
            """

        cursor.execute(sql)

    conn.commit()
    conn.close()


@config.command()
@click.option('--host', prompt='Hostname of the PostgreSQL', help='Hostname of the PostgreSQL instance with optional port')
@click.option('--username', prompt='Instance username', help='Username to login at the PostgreSQL instance')
@click.option('--password', prompt='password', help='Password')
@click.option('--db', prompt='Database', help='Database where broadcaster will keep it information.')
@click.option('--queues', prompt='Queue names', help='A comma-separated list of possible queues.')
def queue(host, username, password, db, queues):
    try:
        conn = pg.connect(f'postgres://{username}:{password}@{host}/{db}')
    except Exception as ex:
        print(f'{ex.__class__.__name__}: {str(ex)}')
        exit(1)

    queue_list = map(lambda c: c.strip(), queues.split(','))

    with conn.cursor() as cursor:
        sql = """
            CREATE SCHEMA IF NOT EXISTS broadcaster;
            DROP TYPE IF EXISTS MessageStatus;
            CREATE TYPE MessageStatus AS ENUM ( 'new', 'delivered', 'success' );
        """

        for queue in queue_list:
            sql += f"""
                CREATE TABLE broadcaster.{queue} (
                    id SERIAL PRIMARY KEY,
                    body TEXT NOT NULL,
                    status MessageStatus DEFAULT 'new'
                );

                CREATE OR REPLACE FUNCTION {queue}_message_arrival_notify()
                    RETURNS trigger AS
                $$
                BEGIN
                    PERFORM pg_notify('{queue}', NEW.id::text);
                    RETURN NEW;
                END;
                $$ LANGUAGE plpgsql;

                CREATE TRIGGER {queue}_message_received
                    AFTER INSERT ON broadcaster.{queue}
                    FOR EACH ROW
                    EXECUTE FUNCTION {queue}_message_arrival_notify();
            """

        cursor.execute(sql)

    conn.commit()
    conn.close()
