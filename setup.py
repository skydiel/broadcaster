from setuptools import setup, find_packages


setup(
    name='broadcaster',
    version='0.1',
    url='https://gitlab.com/skydiel/broadcaster.git',
    license='MIT',
    author='Daniel Cavallo',
    author_email='skydiel@yahoo.com.br',
    description='A simple pub/sub implementation using PostgreSQL as a message broker.',
    keywords='python',
    packages=find_packages(exclude=['broadcaster/tests']),
    long_description=open('README.md').read(),
    include_package_data=True,
    zip_safe=False,
    python_requires=">=3.4",
    install_requires=[
        'click',
        'psycopg2-binary'
    ],
    extras_require={
        'test': [
            'nose',
            'parameterized',
        ]
    },
    entry_points='''
        [console_scripts]
        broadkstr=scripts.config:config
    ''',
)
